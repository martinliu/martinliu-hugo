# Martin Blog source code - 博客代码

Visit my blog at [https://martinliu.cn](https://martinliu.cn) ; this repo is source code

Proudly powered by [Hugo](https://github.com/gohugoio/hugo) ❤️, Theme by [HUGO FUTURE IMPERFECT SLIM](https://themes.gohugo.io/hugo-future-imperfect-slim/).

## ToDo List

* 增加评论功能

## Acknowledgements

- [Cloudfare](https://www.cloudflare.com/): DNS，https 和 静态页面
- [Hugo](https://gohugo.io/): static website builder