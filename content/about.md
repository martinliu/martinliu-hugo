---
date: "2017-03-18T20:53:54+08:00"
title: "劉征"
subtitle: "About Martin Liu"
description: "Martin Liu (刘征) - DevOps and Cloud Computing"
nocomment: true
postmeta: false
seealso: false
bigimg: [{src: "https://res.cloudinary.com/martinliu/image/upload/abstract-2.jpg", desc: ""}]
---

http://www.martinliu.cn 是我的个人博客，开始于2007年。

## 简介

Elastic公司社区布道师，中国DevOps社区核心组织者，《DevOps Handbook》《The Site Reliability Workbook》译者；精通DevOps/SRE/ITSM等理论体系和相关实践等落地实现。致力于在全国范围内通过社区来推广DevOps的理念、技术和实践。推动开源Elastic Stack技术堆栈的应用，包括运维大数据分析平台、云原生服务治理、APM全链路监控和AIOps等使用场景。



----